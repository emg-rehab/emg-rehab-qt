#include "mainwindow.h"
#include "serialworker.h"

#include <QDesktopWidget>
#include <QCloseEvent>
#include <QStyle>
#include <QApplication>
#include <QKeySequence>
#include <QFileDialog>
#include <QMetaObject>
#include <QPen>
#include <QSaveFile>
#include <QFile>
#include <QDataStream>
#include <QTextStream>
#include <QVariant>
#include <QMessageBox>

#include <cmath>
#include <cstdint>
#include <limits>
#include <list>

static const float MIN_TIME_WINDOW = 2.0;

static const char DEFAULT_SAMPLE_RATE_LABEL[] = "Sample Rate: / Hz";
static const char DEFAULT_MIN_RMS_LABEL[] = "Min RMS: / mV";
static const char DEFAULT_MAX_RMS_LABEL[] = "Max RMS: / mV";
static const char DEFAULT_SNR_LABEL[] = "SNR: / dB";

static const unsigned VOLT_TO_WAV_SCALE = 16384;

static const unsigned REPLOT_FREQ = 30;

static const unsigned RMS_WINDOW_SIZE = 500; // in milliseconds

using std::size_t;
using std::list;
using std::exception;
using std::numeric_limits;
using std::sqrt;
using std::log10;

Q_DECLARE_METATYPE(QSerialPortInfo);

/**
 * @brief Convert voltage to WAV sample
 * @param voltage in mV
 * @return WAV sample integer
 */
static qint16 voltageToWavSample(float voltage) {
    long sample = voltage * VOLT_TO_WAV_SCALE; // 1 mV becomes 16384 or 51,2 %
    // Clipping
    if (sample > 32767) {
        sample = 32767;
    }
    else if (sample < -32768) {
        sample = -32768;
    }
    return sample;
}

/**
 * @brief Convert WAV sample to voltage
 * @param WAV sample integer
 * @return voltage in mV
 */
static float wavSampleToVoltage(qint16 sample) {
    return float(sample) / VOLT_TO_WAV_SCALE;
}

void MainWindow::setSampleRate(quint32 sampleRate)
{
    this->sampleRate = sampleRate;
    QString sampleRateStr;
    QTextStream(&sampleRateStr) << "Sample Rate: " << this->sampleRate << " Hz";
    sampleRateLabel.setText(sampleRateStr);
}

void MainWindow::resetSampleRate()
{
    this->sampleRate = 0;
    sampleRateLabel.setText(DEFAULT_SAMPLE_RATE_LABEL);
}

void MainWindow::clear()
{
    bias = 0;
    voltages.clear();

    customPlot.graph(0)->data()->clear();
    customPlot.graph(1)->data()->clear();
    customPlot.xAxis->setRange(0, MIN_TIME_WINDOW);
    customPlot.replot(QCustomPlot::rpQueuedReplot);
    resetSampleRate();
    minRmsLabel.setText(DEFAULT_MIN_RMS_LABEL);
    maxRmsLabel.setText(DEFAULT_MAX_RMS_LABEL);
    snrLabel.setText(DEFAULT_SNR_LABEL);
}

void MainWindow::fitRange()
{
    if (voltages.size()) {
        customPlot.xAxis->setRange(0, double(voltages.size()) / sampleRate);
        customPlot.replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow::addVoltage(float voltage, bool plot)
{
    double t = double(voltages.size()) / sampleRate;
    voltages.push_back(voltage);
    bias += voltage;
    customPlot.graph(0)->addData(t, voltage);
    if (t > MIN_TIME_WINDOW) {
        QCPRange range = customPlot.xAxis->range();
        while (range.upper < t) {
            range.upper *= 2;
            // range += MIN_TIME_WINDOW;
        }
        customPlot.xAxis->setRange(range);
    }
    /* if (t > MIN_TIME_WINDOW) {
        customPlot.xAxis->setRange(0, t);
    } */
    // Replot with REPLOT_FREQ Hz
    if (plot && voltages.size() % (sampleRate / REPLOT_FREQ) == 0) {
        customPlot.replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow::finalize(bool unbias)
{
    fileOpenAction.setDisabled(false);
    fileSaveAsAction.setDisabled(false);
    stopRecordingButton.setDisabled(true);
    fitRangeButton.setDisabled(false);

    if (!voltages.size())
        return;

    customPlot.replot(QCustomPlot::rpQueuedReplot);
    bias /= voltages.size();
    if (unbias) {
        size_t i = 0;
        QCPGraph *graph = customPlot.graph(0);
        customPlot.graph(0)->data()->clear();
        for (float &v : voltages) {
            v -= bias;
            graph->addData(double(i) / sampleRate, v);
            i++;
        }
    }
    list<double> rms;
    float minRms = numeric_limits<float>::infinity();
    float maxRms = 0;

    // Window size in samples
    const size_t windowSize = sampleRate * RMS_WINDOW_SIZE / 1000;
    {
        list<double> envelope_window;
        for (float voltage: voltages) {
            envelope_window.push_back(voltage * voltage);
            if (envelope_window.size() > windowSize) {
                envelope_window.pop_front();
            }
            if (envelope_window.size() == windowSize) {
                double rms_value = 0;
                for (double value : envelope_window) {
                    rms_value += value;
                }
                rms_value = sqrt(rms_value / windowSize);
                if (rms_value < minRms)
                    minRms = rms_value;
                if (rms_value > maxRms)
                    maxRms = rms_value;
                rms.push_back(rms_value);
            }
        }
    }
    {
        QCPGraph *graph = customPlot.graph(1);
        // QSharedPointer<QCPGraphDataContainer> data = graph->data();
        size_t i = 0;
        for (double v : rms) {
            graph->addData(double(i + windowSize / 2) / sampleRate, v);
            i++;
        }
    }
    QString minRmsStr;
    QTextStream(&minRmsStr) << "Min RMS: " << QString::number(minRms, 'f', 4) << " mV";
    minRmsLabel.setText(minRmsStr);

    QString maxRmsStr;
    QTextStream(&maxRmsStr) << "Max RMS: " << QString::number(maxRms, 'f', 4) << " mV";
    maxRmsLabel.setText(maxRmsStr);

    float snr = 20 * log10(maxRms / minRms);
    QString snrStr;
    QTextStream(&snrStr) << "SNR: " << QString::number(snr, 'f', 2) << " dB";
    snrLabel.setText(snrStr);

    customPlot.xAxis->setRange(0, double(voltages.size()) / sampleRate);
    customPlot.replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow::stopSerialWorker()
{
    if (thread) {
        thread->requestInterruption();
        thread = nullptr;
        serialWorker = nullptr;
    }
}

void MainWindow::startRecording()
{
    // Cancel any previously running worker
    stopSerialWorker();

    int portIndex = portComboBox.currentIndex();

    if (portIndex == -1) {
        qWarning("Please select a port!");
    }
    else {
        clear();

        serialWorker = new SerialWorker(portComboBox.currentData().value<QSerialPortInfo>());
        thread = new QThread(this);

        serialWorker->moveToThread(thread);

        connect(thread, &QThread::started, serialWorker, &SerialWorker::run);
        connect(serialWorker, &SerialWorker::stateChanged, this, &MainWindow::serialWorkerState);
        connect(serialWorker, &SerialWorker::readVoltage, this, &MainWindow::readVoltage);
        connect(serialWorker, &SerialWorker::finished, this, &MainWindow::serialWorkerFinished);
        connect(serialWorker, &SerialWorker::finished, thread, &QThread::quit);
        connect(thread, &QThread::finished, this, &MainWindow::waitThread);

        thread->start();

        nRunningThreads++;
        stopRecordingButton.setDisabled(false);
        fileSaveAsAction.setDisabled(true);
        fileOpenAction.setDisabled(true);
        fitRangeButton.setDisabled(true);
    }
}

void MainWindow::stopRecording()
{
    stopSerialWorker();

    statusBar()->showMessage("Stopped.");

    finalize(true);
}

void MainWindow::readVoltage(float voltage)
{
    if (!sender()) {
        qCritical("readVoltage: sender() is NULL!");
        return;
    }

    if (sender() == serialWorker) { // Ignore remaining signals from expired serialWorkers
        addVoltage(voltage, true);
    }
}

void MainWindow::refreshPorts()
{
    portComboBox.clear();
    for (const QSerialPortInfo &serialPortInfo : QSerialPortInfo::availablePorts()) {
        QVariant var;
        var.setValue(serialPortInfo);
        portComboBox.addItem(serialPortInfo.portName(), var);
    }
    portComboBox.setCurrentIndex(-1);
}

void MainWindow::waitThread()
{
    if (!sender()) {
        qCritical("waitThread: sender() is NULL!");
        return;
    }

    QThread *thread = qobject_cast<QThread *>(sender());

    // The thread should be already finished so the following wait call does not block
    if (!thread->wait()) {
        qCritical("Cannot wait thread!"); // Should not happen
    }

    // Schedule thread for final deletion
    thread->deleteLater();

    nRunningThreads--;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (nRunningThreads) {
        statusBar()->showMessage("Cannot close, background threads are running!");
        event->ignore();
    }
    else {
        event->accept();
    }
}

namespace wav_serialization {
    const static quint32 RIFF = 0x46464952; // "RIFF"
    const static quint32 WAVE = 0x45564157; // "WAVE"
    const static quint32 FMT = 0x20746d66; // "fmt "
    const static quint32 FMT_CHUNK_DATA_SIZE = 16;
    const static quint32 DATA = 0x61746164; // "data"

    const static quint16 BITS_PER_SAMPLE = 16; // 16-bit wide sample

    const static quint16 FMT_TYPE_PCM = 1;
    const static quint16 N_CHANNELS = 1;
}

#include <exception>

struct open_file_exception : exception {

};

struct read_exception : open_file_exception {

    const char *what() const noexcept override {
        return "File read error";
    }
};

struct parse_exception : open_file_exception {

    const char *what() const noexcept override {
        return "Cannot parse file";
    }
};

#define THROW_IF_FAILED(stream)  if (stream.status() != QDataStream::Ok) { throw read_exception(); }

void MainWindow::openFile(const QString &path)
{
    using namespace wav_serialization;

    qDebug() << "Opening" << path;
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        statusBar()->showMessage("Cannot open file for reading!");
        return;
    }
    statusBar()->showMessage("Reading file...");
    QDataStream stream(&file);
    stream.setByteOrder(QDataStream::LittleEndian);
    try {
        quint32 riff;
        stream >> riff;
        THROW_IF_FAILED(stream);
        if (riff != RIFF) throw parse_exception();

        quint32 restSize;
        stream >> restSize;
        THROW_IF_FAILED(stream);

        quint32 wave;
        stream >> wave;
        THROW_IF_FAILED(stream);
        if (wave != WAVE) throw parse_exception();

        quint32 fmtId;
        stream >> fmtId;
        THROW_IF_FAILED(stream);
        if (fmtId != FMT) throw parse_exception();

        quint32 fmt_chunk_data_size;
        stream >> fmt_chunk_data_size;
        THROW_IF_FAILED(stream);
        if (fmt_chunk_data_size != FMT_CHUNK_DATA_SIZE) throw parse_exception();

        quint16 format;
        stream >> format;
        THROW_IF_FAILED(stream);
        if (format != FMT_TYPE_PCM) {
            statusBar()->showMessage("Expected PCM format!");
            return;
        }

        quint16 nChannels;
        stream >> nChannels;
        THROW_IF_FAILED(stream);
        if (nChannels != N_CHANNELS) {
            statusBar()->showMessage("Expected only one channel!");
            return;
        }

        quint32 sampleRate;
        stream >> sampleRate;
        THROW_IF_FAILED(stream);

        quint32 byteRate;
        stream >> byteRate;
        THROW_IF_FAILED(stream);

        quint16 blockAlign;
        stream >> blockAlign;
        THROW_IF_FAILED(stream);

        quint16 bitsPerSample;
        stream >> bitsPerSample;
        THROW_IF_FAILED(stream);
        if (bitsPerSample != BITS_PER_SAMPLE) {
            statusBar()->showMessage(QString("Expected %1 bits per sample!").arg(BITS_PER_SAMPLE));
            return;
        }

        quint32 dataId;
        stream >> dataId;
        THROW_IF_FAILED(stream);
        if (dataId != DATA) throw parse_exception();

        quint32 dataSize;
        stream >> dataSize;
        THROW_IF_FAILED(stream);
        if (dataSize == 0) {
            statusBar()->showMessage("Wave file does not have any samples!");
            return;
        }

        // Sanity checks
        if (restSize != dataSize + 36) {
            statusBar()->showMessage("Wave file's size fields do not match!");
            return;
        }

        if (byteRate != (sampleRate * bitsPerSample * nChannels) / 8) {
            statusBar()->showMessage("Wave file's byte rate field is invalid!");
            return;
        }

        if (blockAlign != (BITS_PER_SAMPLE * N_CHANNELS) / 8) {
            statusBar()->showMessage("Wave file's block align field is invalid!");
            return;
        }

        clear();

        fileSaveAsAction.setDisabled(true);
        fitRangeButton.setDisabled(false);

        customPlot.xAxis->setRange(0, double(8 * dataSize / BITS_PER_SAMPLE) / sampleRate);

        setSampleRate(sampleRate);
        qint16 sample;
        quint32 nSamples = dataSize / (bitsPerSample / 8);
        while (nSamples--) {
            stream >> sample;
            THROW_IF_FAILED(stream);
            addVoltage(wavSampleToVoltage(sample), false);
        }
        finalize(false);
        statusBar()->showMessage("File read successfully");
    }
    catch (const open_file_exception &e) {
        statusBar()->showMessage(e.what());
        resetSampleRate();
    }
}

void MainWindow::openFileDialog()
{
    QString format = "Wave File (*.wav *.wave);;Any File (*)";

    /*
     * When closing the file dialog, the following error may be printed:
     * "qt.qpa.xcb: QXcbConnection: XCB error: 3 (BadWindow)"
     *
     * This is a special case in the XCB backend which does not cause
     * any harm and can be ignored.
     *
     * See this bug report: https://bugreports.qt.io/browse/QTBUG-56893
     */
    QString filename = QFileDialog::getOpenFileName(this, "Open File", "", format);

    if (filename.size()) {
        openFile(filename);
    }
    else {
        qDebug() << "Opening file canceled";
    }
}

void MainWindow::saveFileAsDialog()
{
    using namespace wav_serialization;

    QString format = "Wave File (*.wav *.wave);;Any File (*)";

    QString filename = QFileDialog::getSaveFileName(this, "Save File As", "", format);
    if (filename.size()) {
        QSaveFile file(filename);
        if (!file.open(QIODevice::WriteOnly)) {
            statusBar()->showMessage("Cannot open file for writing!");
            return;
        }
        statusBar()->showMessage("Saving file...");
        QDataStream stream(&file);

        quint32 nSamples = voltages.size();
        quint32 dataSize = (nSamples * N_CHANNELS * BITS_PER_SAMPLE) / 8;
        stream.setByteOrder(QDataStream::LittleEndian);
        stream << RIFF << (quint32) (dataSize + 36)
               << WAVE
               << FMT << FMT_CHUNK_DATA_SIZE << FMT_TYPE_PCM
               << N_CHANNELS
               << sampleRate
               << (quint32) ((sampleRate * BITS_PER_SAMPLE * N_CHANNELS) / 8) // (Sample Rate * BitsPerSample * Channels) / 8.
               << (quint16) ((BITS_PER_SAMPLE * N_CHANNELS) / 8) // (BitsPerSample * Channels) / 8
               << BITS_PER_SAMPLE
               << DATA
               << dataSize;
        if (stream.status() != QDataStream::Ok) {
            statusBar()->showMessage("Cannot write to file!");
            return;
        }
        for (float voltage : voltages) {
            stream << voltageToWavSample(voltage);
            if (stream.status() != QDataStream::Ok) {
                statusBar()->showMessage("Cannot write to file!");
                return;
            }
        }
        bool success = file.commit();
        statusBar()->showMessage(success ? "File saved successfully" : "Failed to save file");
    }
    else {
        statusBar()->showMessage("Saving file canceled");
        // qDebug() << "Saving file canceled";
    }
}

void MainWindow::serialWorkerState(SerialWorker::State state)
{
    if (!sender()) {
        qCritical("serialWorkerState: sender() is NULL!");
        return;
    }

    if (sender() == serialWorker) { // Ignore expired serial workers
        typedef SerialWorker::State State;
        QString stateStr;

        switch (state) {
        case State::STARTED:
            stateStr = "Serial worker started...";
            break;

        case State::OPENING_PORT:
            stateStr = "Opening serial port...";
            break;

        case State::SYNCHRONIZING:
            stateStr = "Synchronizing with peripheral...";
            break;

        case State::READING:
            setSampleRate(serialWorker->sampleRate());
            stateStr = "Reading voltages from peripheral...";
            break;

        default:
            stateStr = "Unknown serial worker state!";
        }

        statusBar()->showMessage(stateStr);
    }
}

void MainWindow::serialWorkerFinished(SerialWorker::ResultPtr result)
{
    if (!sender()) {
        qCritical("serialWorkerFinished: sender() is NULL!");
        return;
    }

    if (sender() == serialWorker) { // Ignore expired serial workers
        if (!result->success()) {
            statusBar()->showMessage(QString("Serial session failed: %1").arg(result->what()));
        }
        stopSerialWorker();
        finalize(true);
    }

    // Schedule serialWorker for final deletion
    sender()->deleteLater();
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)

    , voltages()

    , menuBar()
    , fileMenu("&File")
    , fileOpenAction(QApplication::style()->standardIcon(QStyle::SP_DialogOpenButton), "&Open File...")
    , fileSaveAsAction(QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton), "Save &As...")

    , helpMenu("&Help")
    , aboutAction("&About...")

    , centralWidget()
    , layout()

    , portLayout()
    , portLabel("Select Port:")
    , portComboBox()
    , refreshPortsButton("Refresh List of Ports")

    , recordingLayout()
    , startRecordingButton("Record")
    , stopRecordingButton("Stop")
    , fitRangeButton("Zoom to Fit")

    , customPlot()

    , rmsStatslayout()
    , sampleRateLabel(DEFAULT_SAMPLE_RATE_LABEL)
    , minRmsLabel(DEFAULT_MIN_RMS_LABEL)
    , maxRmsLabel(DEFAULT_MAX_RMS_LABEL)
    , snrLabel(DEFAULT_SNR_LABEL)

    , serialWorker()
    , nRunningThreads(0)

    , thread()

    , sampleRate(0)
{
    connect(&refreshPortsButton, &QPushButton::clicked, this, &MainWindow::refreshPorts);

    connect(&fileOpenAction, &QAction::triggered, this, &MainWindow::openFileDialog);
    fileOpenAction.setShortcut(QKeySequence::Open);

    connect(&fileSaveAsAction, &QAction::triggered, this, &MainWindow::saveFileAsDialog);
    fileSaveAsAction.setShortcut(QKeySequence::SaveAs);
    fileSaveAsAction.setDisabled(true);

    fileMenu.addAction(&fileOpenAction);
    fileMenu.addAction(&fileSaveAsAction);

    connect(&aboutAction, &QAction::triggered, this, [this]() {
        QMessageBox::about(this, QString("%1 %2").arg(QApplication::applicationName(), QApplication::applicationVersion()),
                           "A Brain-Computer Interface for Neurorehabilitation.\nThis software can record and analyze EMG signals.");
    });
    helpMenu.addAction(&aboutAction);

    menuBar.addMenu(&fileMenu);
    menuBar.addMenu(&helpMenu);

    setMenuBar(&menuBar);

    portLabel.setFixedWidth(portLabel.fontMetrics().boundingRect(portLabel.text()).width());

    portLayout.addWidget(&portLabel);
    portLayout.addWidget(&portComboBox);
    portLayout.addWidget(&refreshPortsButton);

    stopRecordingButton.setDisabled(true);
    fitRangeButton.setDisabled(true);
    recordingLayout.addWidget(&startRecordingButton);
    recordingLayout.addWidget(&stopRecordingButton);
    recordingLayout.addWidget(&fitRangeButton);

    sampleRateLabel.setFixedHeight(sampleRateLabel.fontMetrics().height());
    rmsStatslayout.addWidget(&sampleRateLabel);
    minRmsLabel.setFixedHeight(minRmsLabel.fontMetrics().height());
    rmsStatslayout.addWidget(&minRmsLabel);
    maxRmsLabel.setFixedHeight(maxRmsLabel.fontMetrics().height());
    rmsStatslayout.addWidget(&maxRmsLabel);
    snrLabel.setFixedHeight(snrLabel.fontMetrics().height());
    rmsStatslayout.addWidget(&snrLabel);

    layout.addLayout(&portLayout);
    layout.addLayout(&recordingLayout);
    layout.addWidget(&customPlot);
    layout.addLayout(&rmsStatslayout);

    centralWidget.setLayout(&layout);

    setWindowTitle("EMG Rehab");
    setCentralWidget(&centralWidget);

    QRect screenGeometry = QDesktopWidget().availableGeometry(this);
    resize(screenGeometry.size() * 0.5);
    int x = (screenGeometry.width() - width()) / 2;
    int y = (screenGeometry.height() - height()) / 2;
    move(x, y);

    connect(&startRecordingButton, &QPushButton::clicked, this, &MainWindow::startRecording);
    connect(&stopRecordingButton, &QPushButton::clicked, this, &MainWindow::stopRecording);
    connect(&fitRangeButton, &QPushButton::clicked, this, &MainWindow::fitRange);

    // give the axes some labels:
    customPlot.xAxis->setLabel("t (s)");
    customPlot.yAxis->setLabel("U (mV)");
    // set axes ranges, so we see all data:
    customPlot.xAxis->setRange(0, MIN_TIME_WINDOW);
    customPlot.yAxis->setRange(-2.0, 2.0);
    // Show legend
    customPlot.legend->setVisible(true);
    // user interaction
    customPlot.setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    QCPAxisRect *axisRect = customPlot.axisRect();
    axisRect->setRangeDrag(Qt::Horizontal);
    axisRect->setRangeZoom(Qt::Horizontal);

    // Antialiasing does not seem to change anything
    // customPlot.setAntialiasedElements(QCP::aeAll);

    QCPGraph *graph;
    // Voltage curve
    graph = customPlot.addGraph();
    graph->setPen(QColorConstants::Svg::blue);
    graph->setName("Signal");
    // RMS curve
    graph = customPlot.addGraph();
    graph->setPen(QColorConstants::Svg::orange);
    graph->setName("RMS");
    // customPlot.graph(0)->setData(x, y);

    customPlot.replot(QCustomPlot::rpQueuedReplot);

    statusBar()->showMessage("Ready.");

    refreshPorts();
}
