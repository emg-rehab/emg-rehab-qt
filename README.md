# EMG Neurorehabilitation Qt
A software for recording and analyzing EMG signals.

# Installation
We are building Linux APT packages on [OBS](https://build.opensuse.org/package/show/home:elaunch/emg-rehab-qt).
The following Linux distributions are currently supported:
- Debian_11
- Raspbian_11

## Add OBS key and repository
If you are not on `Debian_11`, substitute it in the first line with your distribution.
```
DIST=Debian_11
curl -s "https://build.opensuse.org/projects/home:elaunch/public_key" | gpg --dearmor | sudo tee /usr/share/keyrings/obs-elaunch-archive-keyring.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/obs-elaunch-archive-keyring.gpg] https://download.opensuse.org/repositories/home:/elaunch/$DIST/ /" | sudo tee /etc/apt/sources.list.d/obs-elaunch.list
```

## Install package
```
sudo apt update
sudo apt install emg-rehab-qt
```

# How to Use
Connect an Arduino with the [Analog Reader](https://gitlab.com/emg-rehab/analog-reader) program to a USB port of the host. An EMG electrode with an amplifier should be connected to the analog pin of the Arduino. The output of the amplifier should be in the range 0 to 3 Volts. Place the electrode on a muscle.

In the GUI, select the serial port of the Arduino. If you do not know the serial port name of the Arduino, do the following:
1. Disconnect Arduino, click "Refresh List of Ports" and remember the available ports.
2. Connect Arduino and click "Refresh List of Ports". A new serial port will appear. This will be the port of your Arduino.

Click "Record" (with your free hand) to start recording the EMG data. You should see the voltage of the electrode plottet against the time. During the recording, you can keep your muscle relaxed for a few seconds and contract it for a few seconds.

When you are done with the muscle movement click "Stop". The signal and RMS curve will be shown. You can drag the graph with the mouse and zoom it with the mouse wheel. Click "Zoom to Fit" to reset the view. Under the graph you can see information such as the minimum RMS, maximum RMS and a ratio between the two (SNR).

You can save the signal as a WAV file by clicking on "File" and "Save As..." in the menu bar, so you can later open it in the GUI again.
