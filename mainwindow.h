#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QThread>
#include <qcustomplot.h>
#include <QSerialPortInfo>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QPointer>
#include "serialworker.h"
#include <list>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    std::list<float> voltages;

    double bias;

    QMenuBar menuBar;
    QMenu fileMenu;
    QAction fileOpenAction;
    QAction fileSaveAsAction;

    QMenu helpMenu;
    QAction aboutAction;

    QWidget centralWidget;
    QVBoxLayout layout;

    QHBoxLayout portLayout;
    QLabel portLabel;
    QComboBox portComboBox;
    QPushButton refreshPortsButton;

    QHBoxLayout recordingLayout;
    QPushButton startRecordingButton;
    QPushButton stopRecordingButton;
    QPushButton fitRangeButton;

    QCustomPlot customPlot;

    QVBoxLayout rmsStatslayout;
    QLabel sampleRateLabel;
    QLabel minRmsLabel;
    QLabel maxRmsLabel;
    QLabel snrLabel;

    QPointer<SerialWorker> serialWorker;
    unsigned nRunningThreads;
    QPointer<QThread> thread;

    quint32 sampleRate;

    void setSampleRate(quint32 sampleRate);

    void resetSampleRate();

private slots:
    /**
     * @brief Clear recorded data
     */
    void clear();

    /**
     * @brief Zoom time range to fit all the data
     */
    void fitRange();

    //
    /**
     * @brief Add new voltage sample to recording and eventually plot it
     * @param voltage - voltage in mV
     * @param replot - whether to replot the graph
     */
    void addVoltage(float voltage, bool replot);

    /**
     * @brief Finalize recording
     * Optionally remove DC offset from signal and plot RMS envelope.
     * @param unbias - whether to remove DC offset from signal
     */
    void finalize(bool unbias);

    /**
     * @brief Stop serial worker if one is running
     */
    void stopSerialWorker();

    /**
     * @brief Start recording EMG data
     */
    void startRecording();

    /**
     * @brief Stop recording EMG data
     */
    void stopRecording();

    /**
     * @brief Serial worker callback
     */
    void readVoltage(float voltage);

    /**
     * @brief Refresh list of available serial ports
     */
    void refreshPorts();

    /**
     * @brief Wait finished thread
     * Wait (join in POSIX) finished thread.
     * Used as slot for the thread finished signal.
     */
    void waitThread();

    /**
     * @brief Choose file to open in file dialog.
     */
    void openFileDialog();

    /**
     * @brief Save recording as file, choose the path in file dialog.
     */
    void saveFileAsDialog();

    /**
     * @brief Serial worker state changed callback
     * @param state
     */
    void serialWorkerState(SerialWorker::State state);

    /**
     * @brief Serial worker finished callback
     * @param result
     */
    void serialWorkerFinished(SerialWorker::ResultPtr result);

protected:
    void closeEvent(QCloseEvent *event) override;

public:
    explicit MainWindow(QWidget *parent = nullptr);

    /**
     * @brief Open file by path.
     * @param path - file path
     */
    void openFile(const QString& path);
};

#endif // MAINWINDOW_H
