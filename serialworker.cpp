#include "serialworker.h"
#include <list> // QLinkedList is deprecated
// #include <QDebug>
#include <QThread>
#include <QJsonDocument>
#include <QJsonValue>
#include <QtGlobal> // for Q_ASSERT

static const char SYN_WORD[] = "START\r\n";
static const qint32 BAUD_RATE = 1000000;

#define SYN_WORD_LEN (sizeof(SYN_WORD) - 1)

int SerialWorker::_resultPtrTypeId = qRegisterMetaType<SerialWorker::ResultPtr>("ResultPtr");

int SerialWorker::_stateTypeId = qRegisterMetaType<SerialWorker::State>("State");

float SerialWorker::rawToVoltage(int raw)
{
    return (raw - 307) * 5 / 1024.0;
}

void SerialWorker::serialPortError(QSerialPort::SerialPortError error)
{
    if (error != QSerialPort::NoError) {
        emit finished(ResultPtr(new SerialPortError(error)));
        if (_serialPort.isOpen())
            _serialPort.close();
    }
}

SerialWorker::SerialWorker(const QSerialPortInfo &portInfo, QObject *parent)
    : QObject(parent)
    , _serialPort(portInfo, this)
    , _sampleRate(0)
{
    connect(&_serialPort, &QSerialPort::errorOccurred, this, &SerialWorker::serialPortError);
}

void SerialWorker::run()
{
    emit stateChanged(State::STARTED);
    // qDebug("SerialWorker::run()");
    if (!_serialPort.setBaudRate(BAUD_RATE)) {
        // qDebug("Cannot set baud rate!");
        return;
    }
    emit stateChanged(State::OPENING_PORT);
    if (!_serialPort.open(QSerialPort::ReadOnly)) {
        // qDebug("Cannot open port!");
        return;
    }
    emit stateChanged(State::SYNCHRONIZING);
    std::list<char> list;
    bool ready;
    bool interruptRequested;
    while ((interruptRequested = QThread::currentThread()->isInterruptionRequested()) == false &&
           (ready = _serialPort.waitForReadyRead()) == true)
    {
        QByteArray arr = _serialPort.read(1);
        Q_ASSERT(arr.length());
        list.push_back(arr[0]);
        if (list.size() > SYN_WORD_LEN) {
            list.pop_front(); // strip garbage characters from previous session
        }
        if (list.size() == SYN_WORD_LEN) {
            // Check if serial input matches the SYN_WORD
            bool match = true;
            const char *word_iter = SYN_WORD;
            const char * const word_end = SYN_WORD + SYN_WORD_LEN;
            std::list<char>::const_iterator list_iter = list.cbegin();
            do {
                if (*word_iter != *list_iter) {
                    match = false;
                    break;
                }
                word_iter++;
                list_iter++;
            }
            while (word_iter != word_end);
            if (match) {
                break;
            }
        }
    }
    if (interruptRequested) {
        emit finished(ResultPtr(new Terminated));
        return;
    }
    if (!ready) {
        // qWarning("Serial read timed out!");
        return;
    }

    bool headerRead = false;
    // Get JSON information header
    while ((interruptRequested = QThread::currentThread()->isInterruptionRequested()) == false &&
           !headerRead &&
           _serialPort.waitForReadyRead())
    {
        while (!headerRead && _serialPort.canReadLine()) {
            QByteArray array = _serialPort.readLine().trimmed();
            if (array.length()) { // ignore empty lines
                QJsonDocument json = QJsonDocument::fromJson(array);
                if (json.isNull()) {
                    // Cannot parse as JSON.
                    emit finished(ResultPtr(new ParseError));
                    return;
                }
                QJsonValue intervalValue = json["interval"];
                if (intervalValue.isUndefined()) {
                    // Interval key does not exist.
                    emit finished(ResultPtr(new ParseError));
                    return;
                }
                int interval = intervalValue.toInt();
                if (interval <= 0) {
                    // Interval has to be positive.
                    emit finished(ResultPtr(new ParseError));
                    return;
                }
                _sampleRate = 1000000 / interval;
                headerRead = true;
            }
        }
    }
    if (interruptRequested) {
        emit finished(ResultPtr(new Terminated));
        return;
    }
    if (!headerRead) {
        // Read Timeout
        return;
    }

    emit stateChanged(State::READING);
    while ((interruptRequested = QThread::currentThread()->isInterruptionRequested()) == false &&
           _serialPort.waitForReadyRead(5000))
    {
        while (_serialPort.canReadLine()) {
            bool ok;
            QByteArray array = _serialPort.readLine().trimmed();
            if (array.length()) { // ignore empty lines
                int rawValue = array.toInt(&ok);
                if (!ok) {
                    emit finished(ResultPtr(new ParseError));
                    return;
                }
                emit readVoltage(rawToVoltage(rawValue));
            }
        }
    }
    if (interruptRequested) {
        emit finished(ResultPtr(new Terminated));
        return;
    }
    // Otherwise timeout
    // qWarning("Serial read timed out!");
}
