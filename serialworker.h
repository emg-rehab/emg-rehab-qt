#ifndef SERIALWORKER_H
#define SERIALWORKER_H

#include <QSerialPort>
#include <QString>
#include <QMetaType>
#include <QVariant>
#include <QSharedPointer>

class SerialWorker : public QObject
{
    Q_OBJECT

    QSerialPort _serialPort;

    quint32 _sampleRate;

    static float rawToVoltage(int raw);

    static int _resultPtrTypeId;
    static int _stateTypeId;

private slots:
    void serialPortError(QSerialPort::SerialPortError error);

public:
    enum State {
        STARTED,
        OPENING_PORT,
        SYNCHRONIZING,
        READING
    };

    struct Result {
        virtual bool success() const = 0;
        virtual QString what() const = 0;
    };

    typedef QSharedPointer<const Result> ResultPtr;

    struct Terminated : public Result {
        bool success() const override {
            return true;
        }
        QString what() const override {
            return "Thread terminated";
        }
    };

    struct Error : public Result {
        bool success() const override {
            return false;
        }
    };

    class SerialPortError : public Error {
        QString what() const override {
            return QString("Serial port error: %1").arg(error);
        }

    public:
        QSerialPort::SerialPortError error;

        SerialPortError(QSerialPort::SerialPortError error)
            : error(error)
        {}
    };

    struct ParseError : public Error {
        QString what() const override {
            return "Parse error";
        }
    };

    explicit SerialWorker(const QSerialPortInfo &portInfo, QObject *parent = nullptr);

    quint32 sampleRate() const {
        return _sampleRate;
    }

signals:
    void stateChanged(State state);

    void readVoltage(float volt);

    /**
     * @brief signal to be emitted when run() returns.
     * @see run()
     * @param result - either success (Terminated) or fail (Error)
     */
    void finished(ResultPtr result);

public slots:
    void run();
};

Q_DECLARE_METATYPE(SerialWorker::State)
Q_DECLARE_METATYPE(SerialWorker::ResultPtr)

#endif // SERIALWORKER_H
