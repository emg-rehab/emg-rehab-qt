#include "mainwindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include "project-info.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("EMG Rehab");
    QApplication::setApplicationVersion(PROJECT_VERSION);

    QCommandLineParser parser;
    parser.setApplicationDescription("Plot and analyze EMG data, which can be recorded from an Arduino with an electrode.");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("FILE", "EMG signal file to open (optional).");

    parser.process(app);

    const QStringList args = parser.positionalArguments();

    MainWindow w;
    w.show();

    if (args.size()) {
        w.openFile(args[0]);
    }
    return app.exec();
}
